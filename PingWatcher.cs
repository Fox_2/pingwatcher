﻿using ROOT.CIMV2.Win32;
using System;
using System.Diagnostics;
using System.Management;
using System.Net.NetworkInformation;
using System.Threading;
using System.Configuration;

namespace PingWatcher
{
    public class PingWatcher
    {
        private static Ping pinger = new Ping();
        private static bool allowBeep = false;

        public static void Main(string[] args)
        {
            int timeoutMilliseconds = 0, failTimeToResetNetworkAdapter = 0, timeBetweenPings = 0;
            
            try
            {
                if (!int.TryParse(ConfigurationManager.AppSettings["timeout"], out timeoutMilliseconds) ||
                    !int.TryParse(ConfigurationManager.AppSettings["failTimeToResetNetworkAdapter"], out failTimeToResetNetworkAdapter) ||
                    !int.TryParse(ConfigurationManager.AppSettings["timeBetweenPings"], out timeBetweenPings)
                    )
                {
                    throw new Exception("Mindestens einer der Werte in der Konfigurationsdatei ließen sich nicht in Zahlen umwandeln!");
                }
                allowBeep = Convert.ToBoolean(ConfigurationManager.AppSettings["allowBeep"]);
            }
            catch (Exception c)
            {
                Console.WriteLine("Beim  Laden der Configurationsdatei ist ein Fehler aufgetreten!");
                Console.WriteLine(GetFullExceptionMessage(c));
                Console.ReadKey();
                return;
            }

            HandlePing(ConfigurationManager.AppSettings["address"], ConfigurationManager.AppSettings["adapterDescription"], timeoutMilliseconds, failTimeToResetNetworkAdapter, timeBetweenPings);
        }

        private static void HandlePing(string address, string adapterDescription, int timeout, int failTimeToResetNetworkAdapter, int timeBetweenPings)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            while (true)
            {
                try
                {
                    PingReply result = pinger.Send(address, timeout);
                    if (result != null && result.Status == IPStatus.Success)
                    {
                        sw.Reset();
                        sw.Start();
                        Console.WriteLine(result.Status.ToString() + " " + result.RoundtripTime + "ms (" + result.Address + ") TTL=" + result.Options.Ttl);
                    }
                    else
                    {
                        if (sw.ElapsedMilliseconds > failTimeToResetNetworkAdapter)
                        {
                            RestartNetworkAdapter(adapterDescription);
                        }
                        Console.WriteLine("Failed! " + result?.Status.ToString());
                        Beep();
                    }
                }
                catch (Exception c)
                {
                    Console.WriteLine(GetFullExceptionMessage(c));
                    Beep();
                }
                Thread.Sleep(timeBetweenPings);
            }
        }

        private static void RestartNetworkAdapter(string adapterDescription)
        {
            SelectQuery query = new SelectQuery("Win32_NetworkAdapter", "NetConnectionStatus=2");
            ManagementObjectSearcher search = new ManagementObjectSearcher(query);
            foreach (ManagementObject result in search.Get())
            {
                NetworkAdapter adapter = new NetworkAdapter(result);

                if (adapter.Description.Equals(adapterDescription))
                {
                    Console.WriteLine("Restarting Network Adapter " + adapterDescription + "...");
                    adapter.Disable();
                    Thread.Sleep(5000);
                    adapter.Enable();
                    Console.WriteLine("Done");
                }
            }
        }

        private static void Beep()
        {
            if (allowBeep)
            {
                Console.Beep();
            }
        }

        private static string GetFullExceptionMessage(Exception ex)
        {
            string ret = ex.Message;
            while (true)
            {
                if (ex.InnerException == null)
                {
                    break;
                }
                ex = ex.InnerException;
                ret += Environment.NewLine + ex.Message;
            }
            return ret;
        }
    }
}
